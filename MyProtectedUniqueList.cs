﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_26
{
    class MyProtectedUniqueList
    {
        private List<string> _words;
        private string _secret_word;

        public MyProtectedUniqueList(string w)
        {
            _secret_word = w;
            _words = new List<string>();
        }
        public void Add (string w)
        {
            if (string.IsNullOrEmpty(w) == true)
            {
                throw new ArgumentNullException("can't be a null word or empty word");
            }
            else if (_words.Contains(w) == true)
            {
                throw new InvalidOperationException("the word is already exists");
            }
            else
            {
                _words.Add(w);
            }
        }
        public void Remove (string w)
        {
            if (string.IsNullOrEmpty(w) == true)
            {
                throw new ArgumentNullException("can't be a null word or empty word");
            }
            else if (_words.Contains(w) == false)
            {
                throw new ArgumentException("the word is not exists");
            }
            else
            {
                _words.Remove(w);
            }
        }
        public void RemoveAt (int n)
        {
            if (n < 0 || n > _words.Count() - 1)
            {
                throw new ArgumentOutOfRangeException("the number is smaller than 0 or bigger than " + (_words.Count() - 1));
            }
            else
            {
                _words.RemoveAt(n);
            }
        }
        public void Clear (string w)
        {
            if (w == _secret_word)
            {
                _words.Clear();
            }
            else
            {
                throw new AccessViolationException("key word wrong!!");
            }
        }
        public void Sort (string w)
        {
            if (w == _secret_word)
            {
                _words.Sort();
            }
            else
            {
                throw new AccessViolationException("key word wrong!!");
            }
        }

        public override string ToString()
        {
            foreach (string o in _words)
            {
                Console.WriteLine(o);
            }
            return "";
        }
    }
}
