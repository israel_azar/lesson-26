﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_26
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyProtectedUniqueList my = new MyProtectedUniqueList("robert");
                my.Add("yes");
                my.Add("no");
                my.Add("black");
                my.Add("white");
                Console.WriteLine(my);
                my.Add("");
                my.Add("yes");
                my.Remove("");
                my.Remove("blue");
                my.RemoveAt(-2);
                my.RemoveAt(5);
                my.Clear("roberto");
                my.Sort("israel");
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("no empty or null words!!!");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("the word is exists!");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine("you are out of range!!");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("this word is not in this list!");
            }
            catch (AccessViolationException e)
            {
                Console.WriteLine("key word is wrong!!");
            }
        }
    }
}
